import matplotlib.pyplot as plt
import numpy as np


LIM = 30 # Limite do gráfico
NPTS = 20 # Número de pontos coletados
GRAU = 1 # Grau do polinômio

pts = None
# Dados 1:

# pts = [(-21.65, -8.21), (-20.08, -0.91), (-17.3, 5.26), (-13.43, 9.81), (-9.56, 8.51), (-7.62, 4.94), (-4.72, -3.83), (-2.3, -9.68), (0.36, -11.95), (3.27, -9.51), (4.96, -6.27), (7.98, -1.4), (11.25, 2.66), (13.67, 6.88), (18.27, 9.81), (19.6, 7.86), (22.14, 2.01), (24.31, -3.02), (26.49, -8.21), (-18.87, 1.04)]

#Dados 2:
# pts = [(-19.35, -14.0), (-17.37, -9.47), (-15.93, -7.42), (-13.57, -4.62), (-9.53, -1.39), (-5.65, 1.2), (-1.54, 2.28), (2.12, 2.81), (6.68, 2.81), (9.58, 2.81)]

#Dados 3:
# pts = [(-27.7, -3.18), (-25.65, 2.18), (-23.1, 5.42), (-20.32, 2.66), (-18.75, -2.05), (-15.36, -7.4), (-12.46, -9.19), (-10.52, -6.75), (-8.71, -3.02), (-7.26, 0.39), (-4.96, 4.12), (-1.57, 6.4), (0.97, 3.64), (2.66, -0.75), (3.99, -3.34), (6.17, -8.05), (7.62, -10.49), (10.16, -7.4), (12.82, -2.86), (15.24, 1.2)]


if pts:
    NPTS = len(pts)


# Função para aplicar o polinômio
def polinomio(XX):
    valor = 0

    for i in range(GRAU + 1):
        valor += m[i] * (XX ** (GRAU - i ))

    return valor


def truncate(x):
    text = f'{x:.2f}'
    return float(text)


print('Grau: ', GRAU)
if not pts:
    plt.xlim(-LIM, LIM)
    plt.ylim(-LIM, LIM)
    plt.grid()
    pts = plt.ginput(NPTS)

# pts = PTS
for point in range(NPTS):
    pts[point] = truncate(pts[point][0]), truncate(pts[point][1])

print(pts)
plt.close()

pts = np.array(pts)
for i in range(NPTS):
    print(pts[i][0], pts[i][1])

x = pts[:, 0]
y = pts[:, 1]

# A = np.array([x, np.ones(len(x))]).T # Regressão linear
# A = np.array([x**2,x,np.ones(len(x))]).T # Regressão quadrática
# A = np.array([x**3,x**2,x,np.ones(len(x))]).T # Regressão cúbica
# A = np.array([x**4, x**3, x**2, x, np.ones(len(x))]).T # Regressão ordem quatro
A = np.array([x ** (GRAU - i) for i in range(GRAU + 1)]).T # Regressão de qualquer ordem
# print('A:\n', A, '\nFim A')

m = np.matmul(np.matmul(np.linalg.inv(np.matmul(A.T,A)), A.T), y)
# print('m:\n', m, '\nFim M')

# func = lambda XX: m[0]*XX + m[1] # Regressão linear
# func = lambda XX: m[0]*(XX**2) + m[1]*XX + m[2] # Regressão quadrática
# func = lambda XX: m[0]*(XX**3) + m[1]*XX**2 + m[2]*XX + m[3] # Regressão cúbica
# func = lambda XX: m[0]*(XX**4) + m[1]*XX**3 + m[2]*XX**2 + m[3]*XX + m[4] # Regressão ordem quatro
func = polinomio  # Regressão qualquer ordem

X = np.linspace(-LIM, LIM, 200)
Y = func(X)

plt.xlim(-LIM, LIM)
plt.ylim(-LIM, LIM)
plt.plot(x, y, "+")
plt.plot(X, Y)
plt.grid()
plt.show()